package com.test.biradam.batestimtahan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class SelectGroupActivity extends AppCompatActivity {

    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_group);

        Intent intent = getIntent();
        number = intent.getStringExtra("exam_number");

        final Button g1 = (Button) findViewById(R.id.g1);
        g1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    g1.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    g1.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });

        final Button g2 = (Button) findViewById(R.id.g2);
        g2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    g2.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    g2.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });

        final Button g3 = (Button) findViewById(R.id.g3);
        g3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    g3.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    g3.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });

        final Button g4 = (Button) findViewById(R.id.g4);
        g4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    g4.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    g4.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }

    public void g1Press(View view){
        Intent intent = new Intent(getApplicationContext(), ForeignLanguageActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", "1");
        startActivity(intent);
    }

    public void g2Press(View view){
        Intent intent = new Intent(getApplicationContext(), ForeignLanguageActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", "2");
        startActivity(intent);
    }

    public void g3Press(View view){
        Intent intent = new Intent(getApplicationContext(), ForeignLanguageActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", "3");
        startActivity(intent);
    }

    public void g4Press(View view){
        Intent intent = new Intent(getApplicationContext(), ForeignLanguageActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", "4");
        startActivity(intent);
    }

}
