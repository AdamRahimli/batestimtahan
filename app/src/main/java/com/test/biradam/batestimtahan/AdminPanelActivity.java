package com.test.biradam.batestimtahan;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.FirebaseAuth;

public class AdminPanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);


        final Button addQuestionBtn = (Button) findViewById(R.id.addQuestionBtn);
        addQuestionBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    addQuestionBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    addQuestionBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });

        final Button lessonBtn = (Button) findViewById(R.id.lessonBtn);
        lessonBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    lessonBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    lessonBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });

        final Button exitBtn = (Button) findViewById(R.id.exitBtn);
        exitBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    exitBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    exitBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }

    public void addQuestionOnClick(View view){
        Intent intent = new Intent(getApplicationContext(),AddQuestionActivity.class);
        startActivity(intent);
    }

    public void lessonOnClick(View view){
        Intent intent = new Intent(getApplicationContext(),AddLessonActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

    }

    public void exitBtn(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
        startActivity(intent);
    }

}
