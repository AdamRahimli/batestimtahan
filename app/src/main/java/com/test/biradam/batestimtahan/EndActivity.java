package com.test.biradam.batestimtahan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class EndActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        mAuth = FirebaseAuth.getInstance();

        Intent intent = getIntent();
        String score = intent.getStringExtra("score");
        String exam_number = intent.getStringExtra("exam_number");

        TextView textView = findViewById(R.id.textView);
        textView.setText(getString(R.string.score)+": " + score);

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        UUID uuid = UUID.randomUUID();
        myRef.child("Score").child(mAuth.getCurrentUser().getUid()).child(uuid.toString()).child("id").setValue(mAuth.getCurrentUser().getUid());
        myRef.child("Score").child(mAuth.getCurrentUser().getUid()).child(uuid.toString()).child("score").setValue(score);
        myRef.child("Score").child(mAuth.getCurrentUser().getUid()).child(uuid.toString()).child("exam_number").setValue(exam_number);

        final Button backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    backBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    backBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }

    public void backBtn(View view) {
        Intent intent = new Intent(getApplicationContext(), StarterActivity.class);
        startActivity(intent);
    }

}
