package com.test.biradam.batestimtahan;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExamActivity extends AppCompatActivity {

    final String[] type = {"open", "close"};

    static public ArrayList<String> aEnd = new ArrayList<>();
    static public ArrayList<String> qEnd = new ArrayList<>();
    static public HashMap<String, String> aEndHash = new HashMap<>();

    ListView listView;
    QuestionClass adapter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference myRef;

    ArrayList<String> number_exam_list_from_fb; //0,1,2,3,4,5...
    ArrayList<String> name_exam_list_from_fb;
    ArrayList<String> question_id_list_from_fb;
    ArrayList<String> questuin_text_list_from_fb;
    ArrayList<String> question_text_image_list_from_fb;
    ArrayList<ArrayList<String>> question_answers_list_from_fb;
    ArrayList<ArrayList<String>> question_answers_image_list_from_fb;
    ArrayList<String> question_true_answer_list_from_fb;
    ArrayList<String> question_qroup_list_from_fb;  //1,2,3,4
    ArrayList<String> question_lesson_list_from_fb; //az dili, inglis , riy
    ArrayList<String> question_type_list_from_fb; //aciq , qapali

    private int exam_number;
    private int group;

    private ArrayList<String> lessons;
    private HashMap<String, String> scores;
    public static HashMap<String, String> ans;

    CountDownTimer timer;

    @Override
    public void onBackPressed() {

    }

    public void finish(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ExamActivity.this);
        alert.setTitle(getString(R.string.finish));
        alert.setMessage(getString(R.string.are_you_sure));
        alert.setPositiveButton(getString(R.string.finish), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                timer.cancel();
                calScore();
            }
        });
        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);

        lessons = new ArrayList<>();
        scores = new HashMap<>();
        ans = new HashMap<>();

        Intent intent = getIntent();
        exam_number = Integer.valueOf(intent.getStringExtra("exam_number"));
        group = Integer.valueOf(intent.getStringExtra("exam_group"));
        lessons.add(intent.getStringExtra("exam_foreign_language"));

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Group Lesson/" + group);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    HashMap<String, String> hashMap = (HashMap<String, String>) ds.getValue();
                    lessons.add(hashMap.get("lesson"));
                    scores.put(hashMap.get("lesson"), hashMap.get("score"));
                }
                getQuestionList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listView = (ListView) findViewById(R.id.listView);

        number_exam_list_from_fb = new ArrayList<String>();
        name_exam_list_from_fb = new ArrayList<String>();
        question_id_list_from_fb = new ArrayList<String>();
        questuin_text_list_from_fb = new ArrayList<String>();
        question_text_image_list_from_fb = new ArrayList<String>();
        question_answers_list_from_fb = new ArrayList<ArrayList<String>>();
        question_answers_image_list_from_fb = new ArrayList<ArrayList<String>>();
        question_true_answer_list_from_fb = new ArrayList<String>();
        question_qroup_list_from_fb = new ArrayList<String>();
        question_lesson_list_from_fb = new ArrayList<String>();
        question_type_list_from_fb = new ArrayList<String>();

        firebaseDatabase = FirebaseDatabase.getInstance();
        myRef = firebaseDatabase.getReference();

        adapter = new QuestionClass(number_exam_list_from_fb, name_exam_list_from_fb, question_id_list_from_fb, questuin_text_list_from_fb, question_text_image_list_from_fb, question_answers_list_from_fb, question_answers_image_list_from_fb, question_true_answer_list_from_fb, question_qroup_list_from_fb, question_lesson_list_from_fb, question_type_list_from_fb, this);

        listView.setAdapter(adapter);

        getQuestionList();

        timer = new CountDownTimer(1000 * 60 * 60 * 3, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                long lessonTime = millisUntilFinished / 1000;
                String timeStr = String.format("%02d:%02d:%02d", (lessonTime / 3600), (lessonTime % 3600) / 60, (lessonTime % 3600) % 60);
                TextView timerTextView = (TextView) findViewById(R.id.timerTextView);
                timerTextView.setText(timeStr);
            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), getString(R.string.time_s_done), Toast.LENGTH_SHORT).show();
                calScore();
            }
        }.start();
    }

    public void calScore() {
        final HashMap<String, Integer> finalScore = new HashMap<>();
        for (String lesson : lessons) {
            finalScore.put(lesson, 0);
        }
        for (int i = 0; i < qEnd.size(); i++) {
            final String lesson = qEnd.get(i).split("/")[0];
            final String a = aEnd.get(i);
            final int conScore = Integer.valueOf(scores.get(lesson));
            final String trueAns = ans.get(qEnd.get(i).split("/")[1]);
            int oldScore = finalScore.get(lesson);
            if (trueAns.equals(a)) {
                finalScore.put(lesson, oldScore + conScore);
            } else {
                if (qEnd.get(i).split("/")[2].equals("close")) {
                    finalScore.put(lesson, oldScore - conScore / 4);
                }
            }
        }
        int endScore = 0;
        for (String lesson : lessons) {
            if (finalScore.get(lesson) > 0) {
                endScore += finalScore.get(lesson);
            }
        }
        Intent intent = new Intent(getApplicationContext(), EndActivity.class);
        intent.putExtra("score", String.valueOf(endScore));
        intent.putExtra("exam_number", String.valueOf(exam_number));
        startActivity(intent);
    }

    public void getQuestionList() {
        for (String lesson : lessons) {
            for (final String t : type) {
                Query newReference = firebaseDatabase.getReference("Exams/" + exam_number + "/" + lesson + "/" + t).orderByChild("group").equalTo(group);
                newReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            HashMap<String, String> hashMap = (HashMap<String, String>) ds.getValue();

                            number_exam_list_from_fb.add(hashMap.get("exam_number"));
                            name_exam_list_from_fb.add(hashMap.get("exam_number"));
                            question_id_list_from_fb.add(hashMap.get("id"));
                            questuin_text_list_from_fb.add(hashMap.get("text"));
                            question_text_image_list_from_fb.add(hashMap.get("text_image"));

                            if(t.equals("open")){
                                ArrayList<String> answers_list = new ArrayList<>();
                                answers_list.add(hashMap.get("text_additional"));
                                question_answers_list_from_fb.add(answers_list);

                                ArrayList<String> answers_image = new ArrayList<>();
                                answers_image.add(hashMap.get("text_additional_image"));
                                question_answers_image_list_from_fb.add(answers_image);
                            }else if(t.equals("close")){
                                ArrayList<String> answers_list = new ArrayList<>();
                                answers_list.add(hashMap.get("answer_0"));
                                answers_list.add(hashMap.get("answer_1"));
                                answers_list.add(hashMap.get("answer_2"));
                                answers_list.add(hashMap.get("answer_3"));
                                answers_list.add(hashMap.get("answer_4"));
                                question_answers_list_from_fb.add(answers_list);

                                ArrayList<String> answers_image = new ArrayList<>();
                                answers_image.add(hashMap.get("answer_image_0"));
                                answers_image.add(hashMap.get("answer_image_1"));
                                answers_image.add(hashMap.get("answer_image_2"));
                                answers_image.add(hashMap.get("answer_image_3"));
                                answers_image.add(hashMap.get("answer_image_4"));
                                question_answers_image_list_from_fb.add(answers_image);
                            }

                            ans.put(hashMap.get("id"), hashMap.get("true_answer"));

                            question_true_answer_list_from_fb.add(hashMap.get("true_answer"));
                            question_qroup_list_from_fb.add(hashMap.get("group"));
                            question_lesson_list_from_fb.add(hashMap.get("lesson"));
                            question_type_list_from_fb.add(hashMap.get("type"));

                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }

}
