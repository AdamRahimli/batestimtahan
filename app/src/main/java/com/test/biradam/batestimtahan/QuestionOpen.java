package com.test.biradam.batestimtahan;

import android.net.Uri;

public class QuestionOpen {

    private int               number_exam; //0,1,2,3,4,5...
    private String            name_exam;
    private String            question_id;
    private String            questuin_text;
    private Uri               question_text_image;
    private String            questuin_text_additional;
    private Uri               question_text_additional_image;
    private String            question_answer;
    private String            question_true_answer;
    private int               question_qroup;  //1,2,3,4
    private String            question_lesson; //az dili, inglis , riy
    private String            question_type; //aciq , qapali

    public int getNumber_exam() {
        return number_exam;
    }

    public void setNumber_exam(int number_exam) {
        this.number_exam = number_exam;
    }

    public String getName_exam() {
        return name_exam;
    }

    public void setName_exam(String name_exam) {
        this.name_exam = name_exam;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestuin_text() {
        return questuin_text;
    }

    public void setQuestuin_text(String questuin_text) {
        this.questuin_text = questuin_text;
    }

    public Uri getQuestion_text_image() {
        return question_text_image;
    }

    public void setQuestion_text_image(Uri question_text_image) {
        this.question_text_image = question_text_image;
    }

    public String getQuestuin_text_additional() {
        return questuin_text_additional;
    }

    public void setQuestuin_text_additional(String questuin_text_additional) {
        this.questuin_text_additional = questuin_text_additional;
    }

    public Uri getQuestion_text_additional_image() {
        return question_text_additional_image;
    }

    public void setQuestion_text_additional_image(Uri question_text_additional_image) {
        this.question_text_additional_image = question_text_additional_image;
    }

    public String getQuestion_answer() {
        return question_answer;
    }

    public void setQuestion_answer(String question_answer) {
        this.question_answer = question_answer;
    }

    public String getQuestion_true_answer() {
        return question_true_answer;
    }

    public void setQuestion_true_answer(String question_true_answer) {
        this.question_true_answer = question_true_answer;
    }

    public int getQuestion_qroup() {
        return question_qroup;
    }

    public void setQuestion_qroup(int question_qroup) {
        this.question_qroup = question_qroup;
    }

    public String getQuestion_lesson() {
        return question_lesson;
    }

    public void setQuestion_lesson(String question_lesson) {
        this.question_lesson = question_lesson;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }
}
