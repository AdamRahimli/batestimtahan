package com.test.biradam.batestimtahan;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class AddLessonActivity extends AppCompatActivity {

    Spinner group_spinner;
    ListView listView;

    ArrayList<String> lessonIdList;
    ArrayList<String> lessonList;

    String[] group_list;

    int positionPublic = 0;

    ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lesson);

        group_list = new String[]{getString(R.string.select_group), "1", "2", "3", "4", getString(R.string.xarici_dil)};

        lessonList = new ArrayList<>();
        lessonIdList = new ArrayList<>();

        group_spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter gsAA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, group_list);
        gsAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        group_spinner.setAdapter(gsAA);

        listView = (ListView) findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lessonList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                delete(position);
            }
        });

        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionPublic = position;
                refresh(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Button addLessonBtn = (Button) findViewById(R.id.addLessonBtn);
        addLessonBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    addLessonBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    addLessonBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }

    public void refresh(int position) {
        arrayAdapter.clear();
        String dir = "";
        if(position == 0){}
        if(position == 5){dir="Xarici Dil";}
        else{dir=group_list[position];}
        if(!dir.equals("")) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Group Lesson/" + dir);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        HashMap<String, String> hashMap = (HashMap<String, String>) ds.getValue();
                        lessonList.add(hashMap.get("lesson"));
                        lessonIdList.add(hashMap.get("id"));
                        arrayAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void addLesson(View view) {
        TextInputLayout lessonEditText = (TextInputLayout) findViewById(R.id.lessonEditLayText);
        TextInputLayout scoreEditText = (TextInputLayout) findViewById(R.id.scoreEditLayText);
        if (group_spinner.getSelectedItem().toString().equals(getString(R.string.select_group))) {
            Toast.makeText(this, "Select anyone", Toast.LENGTH_SHORT).show();
        } else {
            if (lessonEditText.getEditText().getText().toString().equals("") || scoreEditText.getEditText().getText().toString().equals("")) {
            } else {
                UUID uuid = UUID.randomUUID();
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
                try{
                    if (Integer.valueOf(group_spinner.getSelectedItem().toString()) >= 1 && Integer.valueOf(group_spinner.getSelectedItem().toString()) <= 4) {
                        myRef.child("Group Lesson").child(group_spinner.getSelectedItem().toString()).child(uuid.toString()).child("lesson").setValue(lessonEditText.getEditText().getText().toString());
                        myRef.child("Group Lesson").child(group_spinner.getSelectedItem().toString()).child(uuid.toString()).child("score").setValue(scoreEditText.getEditText().getText().toString());
                        myRef.child("Group Lesson").child(group_spinner.getSelectedItem().toString()).child(uuid.toString()).child("id").setValue(uuid.toString());
                    }
                }catch (Exception ex){
                    myRef.child("Group Lesson").child("Xarici Dil").child(uuid.toString()).child("lesson").setValue(lessonEditText.getEditText().getText().toString());
                    myRef.child("Group Lesson").child("Xarici Dil").child(uuid.toString()).child("score").setValue(scoreEditText.getEditText().getText().toString());
                    myRef.child("Group Lesson").child("Xarici Dil").child(uuid.toString()).child("id").setValue(uuid.toString());
                }
                refresh(positionPublic);
                Toast.makeText(this, "Added", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void delete(final int position) {
        AlertDialog.Builder alert = new AlertDialog.Builder(AddLessonActivity.this);
        alert.setTitle("Delete");
        alert.setMessage("Are you sure?");
        alert.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference dbNode = FirebaseDatabase.getInstance().getReference().getRoot().child("Group Lesson").child(group_spinner.getSelectedItem().toString()).child(lessonIdList.get(position));
                dbNode.setValue(null);
                refresh(positionPublic);
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
        Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
    }

}
