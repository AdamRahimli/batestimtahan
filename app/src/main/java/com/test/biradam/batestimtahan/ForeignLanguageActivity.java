package com.test.biradam.batestimtahan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ForeignLanguageActivity extends AppCompatActivity {

    String number;
    String group;

    ListView listView;

    ArrayList<String> languageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreign_language);

        languageList = new ArrayList<>();

        Intent intent = getIntent();
        number = intent.getStringExtra("exam_number");
        group = intent.getStringExtra("exam_group");

        listView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, languageList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), LookoutActivity.class);
                intent.putExtra("exam_number", String.valueOf(number));
                intent.putExtra("exam_group", String.valueOf(group));
                intent.putExtra("exam_foreign_language", String.valueOf(languageList.get(position)));
                startActivity(intent);
            }
        });

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Group Lesson/Xarici Dil");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    HashMap<String, String> hashMap = (HashMap<String, String>) ds.getValue();
                    languageList.add(hashMap.get("lesson"));
                    arrayAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void engSelect(View view){
        Intent intent = new Intent(getApplicationContext(), LookoutActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", String.valueOf(group));
        intent.putExtra("exam_foreign_language", "eng");
        startActivity(intent);
    }

    public void ruSelect(View view){
        Intent intent = new Intent(getApplicationContext(), LookoutActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", String.valueOf(group));
        intent.putExtra("exam_foreign_language", "ru");
        startActivity(intent);
    }

}
