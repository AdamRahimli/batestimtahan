package com.test.biradam.batestimtahan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class AddQuestionActivity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference myRef;
    private FirebaseAuth mAuth;
    private StorageReference mStorageRef;

    private Question question;
    private static Uri[] question_asnwers_image_sup;

    private QuestionOpen questionOpen;

    String questionType = "";

    ImageView text_img;
    ImageView answer_0_img;
    ImageView answer_1_img;
    ImageView answer_2_img;
    ImageView answer_3_img;
    ImageView answer_4_img;
    ImageView text_additional_img;

    Spinner group_spinner;
    Spinner group_spinner2;
    Spinner lesson_spinner;
    Spinner true_answer_spinner;
    Spinner type_spinner;

    String selectImageName = "";

    String[] group_list;
    String[] group_list2;
    ArrayList<String> lessonList;

    LinearLayout linearLayClose;
    LinearLayout linearLayOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        linearLayClose = (LinearLayout) findViewById(R.id.linearLayClose);
        linearLayOpen = (LinearLayout) findViewById(R.id.linearLayOpen);

        linearLayClose.setVisibility(View.GONE);
        linearLayOpen.setVisibility(View.GONE);

        group_list = new String[]{getString(R.string.select_group), "1", "2", "3", "4", getString(R.string.xarici_dil)};
        group_list2 = new String[]{getString(R.string.select_group), "1", "2", "3", "4"};

        lessonList = new ArrayList<>();

        lesson_spinner = (Spinner) findViewById(R.id.add_question_question_lesson_spinner);
        final ArrayAdapter lsAA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, lessonList);
        lsAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lesson_spinner.setAdapter(lsAA);

        group_spinner = (Spinner) findViewById(R.id.add_question_question_qroup_spinner);
        final ArrayAdapter gsAA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, group_list);
        gsAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        group_spinner.setAdapter(gsAA);

        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (group_list[position].equals(getString(R.string.xarici_dil))) {
                    group_spinner2.setVisibility(View.VISIBLE);
                } else {
                    group_spinner2.setVisibility(View.GONE);
                }
                lsAA.clear();
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Group Lesson/" + group_list[position]);
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            HashMap<String, String> hashMap = (HashMap<String, String>) ds.getValue();
                            lessonList.add(hashMap.get("lesson"));
                            lsAA.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        group_spinner2 = (Spinner) findViewById(R.id.add_question_question_qroup_spinner2);
        group_spinner2.setVisibility(View.INVISIBLE);
        final ArrayAdapter gs2AA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, group_list2);
        gs2AA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        group_spinner2.setAdapter(gs2AA);

        true_answer_spinner = (Spinner) findViewById(R.id.add_question_question_true_answer_spinner);
        ArrayAdapter tasAA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, new String[]{getString(R.string.select_true_answer), "A", "B", "C", "D", "E"});
        tasAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        true_answer_spinner.setAdapter(tasAA);

        final String[] type_list = new String[]{getString(R.string.select_type), getString(R.string.close), getString(R.string.open)};
        type_spinner = (Spinner) findViewById(R.id.add_question_question_type_spinner);
        final ArrayAdapter tAA = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, type_list);
        tAA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type_spinner.setAdapter(tAA);

        type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (type_list[i].equals(getString(R.string.select_type))) {

                } else if (type_list[i].equals(getString(R.string.open))) {
                    linearLayClose.setVisibility(View.GONE);
                    linearLayOpen.setVisibility(View.VISIBLE);
                    questionType = "open";
                } else if (type_list[i].equals(getString(R.string.close))) {
                    linearLayOpen.setVisibility(View.GONE);
                    linearLayClose.setVisibility(View.VISIBLE);
                    questionType = "close";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        text_img = (ImageView) findViewById(R.id.add_question_upload_text_img);
        answer_0_img = (ImageView) findViewById(R.id.add_question_upload_answer_0_img);
        answer_1_img = (ImageView) findViewById(R.id.add_question_upload_answer_1_img);
        answer_2_img = (ImageView) findViewById(R.id.add_question_upload_answer_2_img);
        answer_3_img = (ImageView) findViewById(R.id.add_question_upload_answer_3_img);
        text_additional_img = (ImageView) findViewById(R.id.add_question_upload_text_additional_img);

        question = new Question();
        question_asnwers_image_sup = new Uri[5];

        questionOpen = new QuestionOpen();

        firebaseDatabase = FirebaseDatabase.getInstance();
        myRef = firebaseDatabase.getReference();

        mAuth = FirebaseAuth.getInstance();

        mStorageRef = FirebaseStorage.getInstance().getReference();

        final Button addQuestion = (Button) findViewById(R.id.addQuestion);
        addQuestion.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    addQuestion.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    addQuestion.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }


    public void addQuestion(final Question question) {
        //add question images and take download url
        uploadImage(0);

        if (question.getQuestion_text_image() != null) {
            //add text image and take download url
            UUID uuid = UUID.randomUUID();
            final String imageName = "images/" + question.getNumber_exam() + "/" + question.getQuestion_id() + "/" + uuid + ".jpg";
            final StorageReference storageReference = mStorageRef.child(imageName);
            storageReference.putFile(question.getQuestion_text_image()).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downUri = task.getResult();
                        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("text_image").setValue(downUri.toString());
                    }
                }
            });
        } else if (question.getQuestion_text_image() == null) {
            myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("text_image").setValue("null");
        }

        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("id").setValue(question.getQuestion_id());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("text").setValue(question.getQuestuin_text());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("group").setValue(question.getQuestion_qroup());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("lesson").setValue(question.getQuestion_lesson());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("type").setValue(question.getQuestion_type());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("true_answer").setValue(question.getQuestion_true_answer());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("created_id").setValue(mAuth.getCurrentUser().getUid());
        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("exam_number").setValue(question.getNumber_exam());
        myRef.child("Exams Name").child(String.valueOf(question.getNumber_exam())).child("name").setValue(question.getName_exam());
        myRef.child("Exams Name").child(String.valueOf(question.getNumber_exam())).child("number").setValue(question.getNumber_exam());

    }

    private void uploadImage(final int id) {
        try {
            //upload
            UUID uuid = UUID.randomUUID();
            final String imageName = "images/" + question.getNumber_exam() + "/" + question.getQuestion_id() + "/" + uuid + ".jpg";
            System.out.println("imageName:" + imageName);
            final StorageReference storageReference = mStorageRef.child(imageName);
            storageReference.putFile(question.getQuestion_answers_image_uri()[id]).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    //StorageReference newReference = FirebaseStorage.getInstance().getReference(imageName);
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downUri = task.getResult();
                        myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("answer_image_" + id).setValue(downUri.toString());
                        if (id < 4) {
                            uploadImage(id + 1);
                        }
                    } else {
                        if (task.getException() != null) {
                            Log.i("UPLOAD", "EXCEPTION *** " + task.getException().getLocalizedMessage());
                        }

                        if (id < 4) {
                            uploadImage(id + 1);
                        }
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    if (id < 4) {
                        Log.i("UPLOAD", "NEXT " + (id + 1) + " ON FAILURE" + e.getLocalizedMessage());
                        uploadImage(id + 1);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("answer_image_" + id).setValue("null");

            if (id < 4) {
                uploadImage(id + 1);
            }
        }
        if (question.getQuestion_answers().get(id).length() == 0) {
            myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("answer_" + id).setValue("");
        } else if (question.getQuestion_answers().get(id).length() != 0) {
            myRef.child("Exams").child(String.valueOf(question.getNumber_exam())).child(question.getQuestion_lesson()).child(question.getQuestion_type()).child(question.getQuestion_id()).child("answer_" + id).setValue(question.getQuestion_answers().get(id));
        }
    }

    public void upload(View view) {
        if (questionType.equals("close")) {
            try {
                TextInputLayout number_exam = (TextInputLayout) findViewById(R.id.add_question_number_exam_edit_text);
                TextInputLayout name_exam = (TextInputLayout) findViewById(R.id.add_question_name_exam_edit_text);
                TextInputLayout questuin_text = (TextInputLayout) findViewById(R.id.add_question_questuin_text_edit_text);
                TextInputLayout question_answer_0 = (TextInputLayout) findViewById(R.id.add_question_question_answer_edit_text);
                TextInputLayout question_answer_1 = (TextInputLayout) findViewById(R.id.add_question_question_answer_1_edit_text);
                TextInputLayout question_answer_2 = (TextInputLayout) findViewById(R.id.add_question_question_answer_2_edit_text);
                TextInputLayout question_answer_3 = (TextInputLayout) findViewById(R.id.add_question_question_answer_3_edit_text);
                TextInputLayout question_answer_4 = (TextInputLayout) findViewById(R.id.add_question_question_answer_4_edit_text);
                //EditText question_true_answer = (EditText) findViewById(R.id.add_question_question_true_answer_edit_text);
                //EditText question_qroup = (EditText) findViewById(R.id.add_question_question_qroup_edit_text);
                //EditText question_lesson = (EditText) findViewById(R.id.add_question_question_lesson_edit_text);
                //TextInputLayout question_type = (TextInputLayout) findViewById(R.id.add_question_question_type_edit_text);


                if (!number_exam.getEditText().getText().toString().isEmpty()
                        && !name_exam.getEditText().getText().toString().isEmpty()
                        && !questuin_text.getEditText().getText().toString().isEmpty()
                    //&& !question_true_answer.getText().toString().isEmpty()
                    //&& !question_qroup.getText().toString().isEmpty()
                    //&& !question_lesson.getText().toString().isEmpty()
                    /*&& !question_type.getEditText().getText().toString().isEmpty() */) {

                    UUID uuid = UUID.randomUUID();
                    question.setQuestion_id(uuid.toString());
                    question.setNumber_exam(Integer.valueOf(number_exam.getEditText().getText().toString()));
                    question.setName_exam(name_exam.getEditText().getText().toString());
                    question.setQuestuin_text(questuin_text.getEditText().getText().toString());

                    ArrayList<String> question_answers = new ArrayList<>();
                    question_answers.add(question_answer_0.getEditText().getText().toString());
                    question_answers.add(question_answer_1.getEditText().getText().toString());
                    question_answers.add(question_answer_2.getEditText().getText().toString());
                    question_answers.add(question_answer_3.getEditText().getText().toString());
                    question_answers.add(question_answer_4.getEditText().getText().toString());
                    question.setQuestion_answers(question_answers);

                    question.setQuestion_true_answer(String.valueOf(true_answer_spinner.getSelectedItemId()));
                    //xarici dil duzelt
                    if (group_spinner.getSelectedItem().toString().equals(getString(R.string.xarici_dil))) {
                        question.setQuestion_qroup(Integer.valueOf(group_spinner2.getSelectedItem().toString()));
                    } else {
                        question.setQuestion_qroup(Integer.valueOf(group_spinner.getSelectedItem().toString()));
                    }

                    question.setQuestion_lesson(lesson_spinner.getSelectedItem().toString());
                    if (type_spinner.getSelectedItem().toString().equals(getString(R.string.open))) {
                        question.setQuestion_type("open");
                    } else if (type_spinner.getSelectedItem().toString().equals(getString(R.string.close))) {
                        question.setQuestion_type("close");
                    }
//                    question.setQuestion_type(question_type.getEditText().getText().toString());
                    question.setQuestion_answers_image_uri(question_asnwers_image_sup);

                    addQuestion(question);

                    Intent intent = new Intent(getApplicationContext(), AdminPanelActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
            }

        } else if (questionType.equals("open")) {
            try {
                TextInputLayout questuin_text_additional = (TextInputLayout) findViewById(R.id.add_question_question_text_additional_open_edit_text);
                TextInputLayout answer = (TextInputLayout) findViewById(R.id.add_question_question_answer_open_edit_text);
                TextInputLayout number_exam = (TextInputLayout) findViewById(R.id.add_question_number_exam_edit_text);
                TextInputLayout name_exam = (TextInputLayout) findViewById(R.id.add_question_name_exam_edit_text);
                TextInputLayout questuin_text = (TextInputLayout) findViewById(R.id.add_question_questuin_text_edit_text);

                UUID uuid = UUID.randomUUID();
                questionOpen.setQuestion_id(uuid.toString());
                questionOpen.setQuestuin_text_additional(questuin_text_additional.getEditText().getText().toString());
                questionOpen.setQuestion_answer(answer.getEditText().getText().toString());
                questionOpen.setNumber_exam(Integer.valueOf(number_exam.getEditText().getText().toString()));
                questionOpen.setName_exam(name_exam.getEditText().getText().toString());
                questionOpen.setQuestuin_text(questuin_text.getEditText().getText().toString());

                if (group_spinner.getSelectedItem().toString().equals(getString(R.string.xarici_dil))) {
                    questionOpen.setQuestion_qroup(Integer.valueOf(group_spinner2.getSelectedItem().toString()));
                } else {
                    questionOpen.setQuestion_qroup(Integer.valueOf(group_spinner.getSelectedItem().toString()));
                }

                questionOpen.setQuestion_lesson(lesson_spinner.getSelectedItem().toString());
                if (type_spinner.getSelectedItem().toString().equals(getString(R.string.open))) {
                    questionOpen.setQuestion_type("open");
                } else if (type_spinner.getSelectedItem().toString().equals(getString(R.string.close))) {
                    questionOpen.setQuestion_type("close");
                }

                addQuestionOpen(questionOpen);

                Intent intent = new Intent(getApplicationContext(), AdminPanelActivity.class);
                startActivity(intent);
            } catch (Exception ex) {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                ex.printStackTrace();
            }

        }
    }

    public void addQuestionOpen(final QuestionOpen questionOpen) {
        if (questionOpen.getQuestion_text_image() != null) {
            //add text image and take download url
            UUID uuid = UUID.randomUUID();
            final String imageName = "images/" + questionOpen.getNumber_exam() + "/" + questionOpen.getQuestion_id() + "/" + uuid + ".jpg";
            final StorageReference storageReference = mStorageRef.child(imageName);
            storageReference.putFile(questionOpen.getQuestion_text_image()).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downUri = task.getResult();
                        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text_image").setValue(downUri.toString());
                    }
                }
            });
        } else if (questionOpen.getQuestion_text_image() == null) {
            myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text_image").setValue("null");
        }

        if (questionOpen.getQuestion_text_additional_image() != null) {
            //add text image and take download url
            UUID uuid = UUID.randomUUID();
            final String imageName = "images/" + questionOpen.getNumber_exam() + "/" + questionOpen.getQuestion_id() + "/" + uuid + ".jpg";
            final StorageReference storageReference = mStorageRef.child(imageName);
            storageReference.putFile(questionOpen.getQuestion_text_additional_image()).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return storageReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downUri = task.getResult();
                        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text_additional_image").setValue(downUri.toString());
                    }
                }
            });
        } else if (questionOpen.getQuestion_text_image() == null) {
            myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text_additional_image").setValue("null");
        }

        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("id").setValue(questionOpen.getQuestion_id());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text").setValue(questionOpen.getQuestuin_text());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("group").setValue(questionOpen.getQuestion_qroup());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("text_additional").setValue(questionOpen.getQuestuin_text_additional());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("lesson").setValue(questionOpen.getQuestion_lesson());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("type").setValue(questionOpen.getQuestion_type());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("true_answer").setValue(questionOpen.getQuestion_answer());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("created_id").setValue(mAuth.getCurrentUser().getUid());
        myRef.child("Exams").child(String.valueOf(questionOpen.getNumber_exam())).child(questionOpen.getQuestion_lesson()).child(questionOpen.getQuestion_type()).child(questionOpen.getQuestion_id()).child("exam_number").setValue(questionOpen.getNumber_exam());
        myRef.child("Exams Name").child(String.valueOf(questionOpen.getNumber_exam())).child("name").setValue(questionOpen.getName_exam());
        myRef.child("Exams Name").child(String.valueOf(questionOpen.getNumber_exam())).child("number").setValue(questionOpen.getNumber_exam());

    }

    public void selectTextImage(View view) {
        selectImageName = "selectTextImage";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAnswer0Image(View view) {
        selectImageName = "selectAnswer0Image";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAnswer1Image(View view) {
        selectImageName = "selectAnswer1Image";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAnswer2Image(View view) {
        selectImageName = "selectAnswer2Image";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAnswer3Image(View view) {
        selectImageName = "selectAnswer3Image";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAnswer4Image(View view) {
        selectImageName = "selectAnswer4Image";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    public void selectAdditionalTextImage(View view) {
        selectImageName = "selectAdditionalTextImage";
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, 2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 2 && resultCode == RESULT_OK && data != null) {
            try {
                Uri image = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), image);
                if (selectImageName.equals("selectTextImage")) {
                    question.setQuestion_text_image(image);
                    questionOpen.setQuestion_text_image(image);
                    text_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAnswer0Image")) {
                    question_asnwers_image_sup[0] = image;
                    answer_0_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAnswer1Image")) {
                    question_asnwers_image_sup[1] = image;
                    answer_1_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAnswer2Image")) {
                    question_asnwers_image_sup[2] = image;
                    answer_2_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAnswer3Image")) {
                    question_asnwers_image_sup[3] = image;
                    answer_3_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAnswer4Image")) {
                    question_asnwers_image_sup[4] = image;
                    answer_4_img.setImageBitmap(bitmap);
                } else if (selectImageName.equals("selectAdditionalTextImage")) {
                    questionOpen.setQuestion_text_additional_image(image);
                    text_additional_img.setImageBitmap(bitmap);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
