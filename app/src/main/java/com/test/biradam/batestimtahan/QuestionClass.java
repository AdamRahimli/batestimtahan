package com.test.biradam.batestimtahan;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class QuestionClass extends ArrayAdapter<String> {

    private final ArrayList<String> number_exam_list; //0,1,2,3,4,5...
    private final ArrayList<String> name_exam_list;
    private final ArrayList<String> question_id_list;
    private final ArrayList<String> questuin_text_list;
    private final ArrayList<String> question_text_image_list;
    private final ArrayList<ArrayList<String>> question_answers_list;
    private final ArrayList<ArrayList<String>> question_answers_image_list;
    private final ArrayList<String> question_true_answer_list;
    private final ArrayList<String> question_qroup_list;  //1,2,3,4
    private final ArrayList<String> question_lesson_list; //az dili, inglis , riy
    private final ArrayList<String> question_type_list; //aciq , qapali
    private final Activity context;


    public QuestionClass(ArrayList<String> number_exam_list, ArrayList<String> name_exam_list, ArrayList<String> question_id_list, ArrayList<String> questuin_text_list, ArrayList<String> question_text_image_list, ArrayList<ArrayList<String>> question_answers_list, ArrayList<ArrayList<String>> question_answers_image_list, ArrayList<String> question_true_answer_list, ArrayList<String> question_qroup_list, ArrayList<String> question_lesson_list, ArrayList<String> question_type_list, Activity context) {
        super(context, R.layout.custom_view, questuin_text_list);
        this.number_exam_list = number_exam_list;
        this.name_exam_list = name_exam_list;
        this.question_id_list = question_id_list;
        this.questuin_text_list = questuin_text_list;
        this.question_text_image_list = question_text_image_list;
        this.question_answers_list = question_answers_list;
        this.question_answers_image_list = question_answers_image_list;
        this.question_true_answer_list = question_true_answer_list;
        this.question_qroup_list = question_qroup_list;
        this.question_lesson_list = question_lesson_list;
        this.question_type_list = question_type_list;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();
        View customView = layoutInflater.inflate(R.layout.custom_view, null, true);

        TextView question_number = customView.findViewById(R.id.custum_view_question_number);
        question_number.setText(String.valueOf(position + 1) + ".");

        TextView question_text = customView.findViewById(R.id.custum_view_question_text);
        ImageView question_text_image = customView.findViewById(R.id.custum_view_question_text_image);

        if (questuin_text_list.get(position).equals("")) {
            question_text.setVisibility(View.GONE);
        } else {
            question_text.setVisibility(View.VISIBLE);
            question_text.setText(questuin_text_list.get(position));
        }

        if (question_text_image_list.get(position).equals("null")) {
            question_text_image.setVisibility(View.GONE);
        } else {
            question_text_image.setVisibility(View.VISIBLE);
            Picasso.get().load(question_text_image_list.get(position)).into(question_text_image);
        }

        LinearLayout linearLayoutClose = (LinearLayout) customView.findViewById(R.id.linearLayCloseCV);
        LinearLayout linearLayoutOpen = (LinearLayout) customView.findViewById(R.id.linearLayOpenCV);

        if (question_type_list.get(position).equals("open")) {
            linearLayoutClose.setVisibility(View.GONE);
            linearLayoutOpen.setVisibility(View.VISIBLE);

            final EditText answer_edit_text = customView.findViewById(R.id.custum_view_question_answer);

            answer_edit_text.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    ExamActivity.aEnd.add(answer_edit_text.getText().toString());
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), answer_edit_text.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            TextView text_additional = customView.findViewById(R.id.custum_view_question_text_additional);
            if (question_answers_list.get(position).get(0).equals("")) {
                text_additional.setVisibility(View.GONE);
            } else {
                text_additional.setVisibility(View.VISIBLE);
                text_additional.setText(question_answers_list.get(position).get(0));
            }

            ImageView text_additional_image = customView.findViewById(R.id.custum_view_question_text_additional_image);
            if (question_answers_image_list.get(position).get(0).equals("null")) {
                text_additional_image.setVisibility(View.GONE);
            } else {
                text_additional_image.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(0)).into(text_additional_image);
            }

        } else if (question_type_list.get(position).equals("close")) {
            linearLayoutClose.setVisibility(View.VISIBLE);
            linearLayoutOpen.setVisibility(View.GONE);

            final RadioButton question_answer_A = customView.findViewById(R.id.custon_view_question_answer_A);
            final RadioButton question_answer_B = customView.findViewById(R.id.custon_view_question_answer_B);
            final RadioButton question_answer_C = customView.findViewById(R.id.custon_view_question_answer_C);
            final RadioButton question_answer_D = customView.findViewById(R.id.custon_view_question_answer_D);
            final RadioButton question_answer_E = customView.findViewById(R.id.custon_view_question_answer_E);

            question_answer_A.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    question_answer_A.setChecked(true);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                    ExamActivity.aEnd.add("0");
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), "0");
                }
            });

            question_answer_B.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(true);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                    ExamActivity.aEnd.add("1");
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), "1");
                }
            });

            question_answer_C.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(true);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                    ExamActivity.aEnd.add("2");
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), "2");
                }
            });

            question_answer_D.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(true);
                    question_answer_E.setChecked(false);
                    ExamActivity.aEnd.add("3");
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), "3");
                }
            });

            question_answer_E.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(true);
                    ExamActivity.aEnd.add("4");
                    ExamActivity.qEnd.add(question_lesson_list.get(position) + "/" + question_id_list.get(position)+"/"+question_type_list.get(position));
                    ExamActivity.aEndHash.put(question_id_list.get(position), "4");
                }
            });

            try {
                int a = Integer.valueOf(ExamActivity.aEndHash.get(question_id_list.get(position)));
                if (a == 0) {
                    question_answer_A.setChecked(true);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                } else if (a == 1) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(true);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                } else if (a == 2) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(true);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(false);
                } else if (a == 3) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(true);
                    question_answer_E.setChecked(false);
                } else if (a == 4) {
                    question_answer_A.setChecked(false);
                    question_answer_B.setChecked(false);
                    question_answer_C.setChecked(false);
                    question_answer_D.setChecked(false);
                    question_answer_E.setChecked(true);
                }
            } catch (Exception ex) {

            }
            TextView question_answer_0 = customView.findViewById(R.id.custum_view_question_answer_0);
            ImageView question_answer_image_0 = customView.findViewById(R.id.custum_view_question_answer_image_0);
            TextView question_answer_1 = customView.findViewById(R.id.custum_view_question_answer_1);
            ImageView question_answer_image_1 = customView.findViewById(R.id.custum_view_question_answer_image_1);
            TextView question_answer_2 = customView.findViewById(R.id.custum_view_question_answer_2);
            ImageView question_answer_image_2 = customView.findViewById(R.id.custum_view_question_answer_image_2);
            TextView question_answer_3 = customView.findViewById(R.id.custum_view_question_answer_3);
            ImageView question_answer_image_3 = customView.findViewById(R.id.custum_view_question_answer_image_3);
            TextView question_answer_4 = customView.findViewById(R.id.custum_view_question_answer_4);
            ImageView question_answer_image_4 = customView.findViewById(R.id.custum_view_question_answer_image_4);

            if (question_answers_list.get(position).get(0).equals("")) {
                question_answer_0.setVisibility(View.GONE);
            } else {
                question_answer_0.setVisibility(View.VISIBLE);
                question_answer_0.setText(question_answers_list.get(position).get(0));
            }

            if (question_answers_image_list.get(position).get(0).equals("null")) {
                question_answer_image_0.setVisibility(View.GONE);
            } else {
                question_answer_image_0.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(0)).into(question_answer_image_0);
            }

            if (question_answers_list.get(position).get(1).equals("")) {
                question_answer_1.setVisibility(View.GONE);
            } else {
                question_answer_1.setVisibility(View.VISIBLE);
                question_answer_1.setText(question_answers_list.get(position).get(1));
            }

            if (question_answers_image_list.get(position).get(1).equals("null")) {
                question_answer_image_1.setVisibility(View.GONE);
            } else {
                question_answer_image_1.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(1)).into(question_answer_image_1);
            }

            if (question_answers_list.get(position).get(2).equals("")) {
                question_answer_2.setVisibility(View.GONE);
            } else {
                question_answer_2.setVisibility(View.VISIBLE);
                question_answer_2.setText(question_answers_list.get(position).get(2));
            }

            if (question_answers_image_list.get(position).get(2).equals("null")) {
                question_answer_image_2.setVisibility(View.GONE);
            } else {
                question_answer_image_2.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(2)).into(question_answer_image_2);
            }

            if (question_answers_list.get(position).get(3).equals("")) {
                question_answer_3.setVisibility(View.GONE);
            } else {
                question_answer_3.setVisibility(View.VISIBLE);
                question_answer_3.setText(question_answers_list.get(position).get(3));
            }

            if (question_answers_image_list.get(position).get(3).equals("null")) {
                question_answer_image_3.setVisibility(View.GONE);
            } else {
                question_answer_image_3.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(3)).into(question_answer_image_3);
            }

            if (question_answers_list.get(position).get(4).equals("")) {
                question_answer_4.setVisibility(View.GONE);
            } else {
                question_answer_4.setVisibility(View.VISIBLE);
                question_answer_4.setText(question_answers_list.get(position).get(4));
            }

            if (question_answers_image_list.get(position).get(4).equals("null")) {
                question_answer_image_4.setVisibility(View.GONE);
            } else {
                question_answer_image_4.setVisibility(View.VISIBLE);
                Picasso.get().load(question_answers_image_list.get(position).get(4)).into(question_answer_image_4);
            }


        }



        return customView;
    }
}
