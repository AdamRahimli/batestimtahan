package com.test.biradam.batestimtahan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LookoutActivity extends AppCompatActivity {

    String number;
    String group;
    String exam_foreign_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookout);

        Intent intent = getIntent();
        number = intent.getStringExtra("exam_number");
        group = intent.getStringExtra("exam_group");
        exam_foreign_language = intent.getStringExtra("exam_foreign_language");

        final Button startBtn = (Button) findViewById(R.id.startBtn);
        startBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    startBtn.setBackground(getDrawable(R.drawable.onclick_outlined_button_style));
                } else if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    startBtn.setBackground(getDrawable(R.drawable.default_outlined_button_style));
                }
                return false;
            }
        });
    }

    public void startExam(View view){
        Intent intent = new Intent(getApplicationContext(), ExamActivity.class);
        intent.putExtra("exam_number", String.valueOf(number));
        intent.putExtra("exam_group", String.valueOf(group));
        intent.putExtra("exam_foreign_language", String.valueOf(exam_foreign_language));
        startActivity(intent);
    }

}
